package sample;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.logging.Logger;

public class Main extends Application {
    AnchorPane root = new AnchorPane();

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Hello World");
       // primaryStage.setScene(new Scene(root, 300, 275));
        //primaryStage.setFullScreen(true);
        Screen screen = Screen.getPrimary();
        Rectangle2D boundsStage = screen.getVisualBounds();
        primaryStage.setX(0);
        primaryStage.setY(0);
        primaryStage.setWidth(boundsStage.getWidth());
        primaryStage.setHeight(boundsStage.getHeight());
        primaryStage.setMaximized(true);
   //     primaryStage.setFullScreen(true);
        primaryStage.setScene(new Scene(root));
        loadSplashScreen();
        primaryStage.show();

    }

    private void loadSplashScreen() {
        try {

            StackPane pane = FXMLLoader.load(getClass().getResource(("/Welcome.fxml")));
            root.getChildren().setAll(pane);
            AnchorPane.setTopAnchor(pane,0.0);
            AnchorPane.setBottomAnchor(pane,0.0);
            AnchorPane.setLeftAnchor(pane,0.0);
            AnchorPane.setRightAnchor(pane,0.0);
            FadeTransition fadeIn = new FadeTransition(Duration.seconds(2), pane);
            fadeIn.setFromValue(0);
            fadeIn.setToValue(1);
            fadeIn.setCycleCount(1);

            FadeTransition fadeOut = new FadeTransition(Duration.seconds(2), pane);
            fadeOut.setFromValue(1);
            fadeOut.setToValue(0);
            fadeOut.setCycleCount(1);

            fadeIn.play();

            fadeIn.setOnFinished((e) -> {
                fadeOut.play();
            });

            fadeOut.setOnFinished(this::handle);

        } catch (IOException ex) {
            System.err.println("erooorr 1");        }
    }
    public static void main(String[] args) {
        launch(args);
    }

    private void handle(ActionEvent e) {
        try {
            AnchorPane pane =FXMLLoader.load(getClass().getResource("/Home.fxml"));
                    root.getChildren().setAll(pane);

            AnchorPane.setTopAnchor(pane,0.0);
            AnchorPane.setBottomAnchor(pane,0.0);
            AnchorPane.setLeftAnchor(pane,0.0);
            AnchorPane.setRightAnchor(pane,0.0);
        } catch (IOException ex) {
            System.err.println("eroorrr");
        }
    }
}
