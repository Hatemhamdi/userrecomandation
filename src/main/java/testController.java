import Category.CategoryDao;
import com.jfoenix.controls.JFXButton;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import object.Similarity;
import object.Trust;
import object.testTable;


import java.net.URL;
import java.util.*;

public class testController implements Initializable {

    @FXML
    private TableView<testTable> testRecommendation;

    @FXML
    private TableColumn<testTable, String> idUser;

    @FXML
    private TableColumn<testTable, String> idProduct;

    @FXML
    private TableColumn<testTable, String> realRating;

    @FXML
    private TableColumn<testTable, String> predictRating;

    @FXML
    private TableColumn<testTable, String> predictionIndex;

    @FXML
    private TableColumn<testTable, String> p5;

    @FXML
    private JFXButton submitButton;

    Float baseMin= new Float(0.0);
    Float baseMax= new Float(0.0);

    Float baseMinP5= new Float(0.0);
    Float baseMaxP5= new Float(0.0);

    Float limitMin=new Float(0.0);
    Float limitMax=new Float(5.0);
@FXML
Label txtMae;
    @FXML
    Label  txtRMSE;

    CategoryDao catDao = new CategoryDao();
    private float mae = new Float(0.0);
    private float rmse = new Float(0.0);

    private float maeP5 = new Float(0.0);
    private float rmseP5 = new Float(0.0);

    @Override
    protected void finalize() throws Throwable {


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initColumn();
        submitButton.setOnAction(action->{
            testRecommendation.setItems(catDao.getAllTestTable());
            getMinMax(testRecommendation.getItems());
            updatePredictColumn();
            calculMae();
        });

    }

    private void calculMae() {
    List<Float> list = new ArrayList<Float>();
        List<Float> listP5 = new ArrayList<Float>();
        testRecommendation.getItems().forEach(e->{
        if(!Float.isNaN(e.getPredictRate())){
            mae+=Math.abs(e.getRealRate()-e.getPredictRate());
            rmse+=Math.pow(e.getRealRate()-e.getPredictRate(),2);
            list.add(Math.abs(e.getRealRate()-e.getPredictRate()));

            maeP5+=Math.abs(e.getRealRate()-e.getP5());
            rmseP5+=Math.pow(e.getRealRate()-e.getP5(),2);
            listP5.add(Math.abs(e.getRealRate()-e.getP5()));
        }
    });
        txtMae.setText("MAE P4 : "+mae/list.size()+"\nMAE P1 : "+maeP5/listP5.size());
        txtRMSE.setText("RMSE P4 : "+Math.sqrt(rmse/list.size())+"\nRMSE P1 : "+Math.sqrt(rmseP5/listP5.size()));

    }

    private void updatePredictColumn() {
        System.err.println("FIIIIIIIN ========================> updatePredictColumn");

        testRecommendation.getItems().forEach(e->{
            e.setPredictRate(((limitMax - limitMin) * (e.getPredictRate() - baseMin) / (baseMax - baseMin)) + limitMin);
            e.setP5(((limitMax - limitMin) * (e.getP5()- baseMinP5) / (baseMaxP5 - baseMinP5)) + limitMin);
        });
    }

    private void getMinMax(ObservableList<testTable> items) {
        //List<Float> list = new ArrayList<Float>();

       /* items.forEach(e->{
            //if(!Float.isNaN(e.getPredictRate()))
            list.add(new Float(e.getPredictRate()));
        });
        list.sort(Comparator.naturalOrder());
limitMin=list.get(0);
limitMax=list.get(list.size()-1);*/
        items.forEach(e->{
            if(!Float.isNaN(e.getPredictRate())) {
                if (e.getPredictRate() > baseMax)
                    baseMax = e.getPredictRate();
                if (e.getPredictRate() < baseMin)
                    baseMin = e.getPredictRate();
            }
            if(!Float.isNaN(e.getP5())) {
                if (e.getP5() > baseMaxP5)
                    baseMaxP5 = e.getP5();
                if (e.getP5()< baseMinP5)
                    baseMinP5 = e.getP5();
            }
        });
        System.err.println("FIIIIIIIN ========================> GET MIN MAX   " +baseMinP5+" MAX : "+baseMaxP5);
    }

    private void initColumn() {
        idUser.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdUser() != null) {
                object.setValue(param.getValue().getIdUser().toString());
            }
            return object;
        });
        idProduct.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getProdId() != null) {
                object.setValue(param.getValue().getProdId().toString());
            }
            return object;

        });

        realRating.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getRealRate() != null) {
                object.setValue(param.getValue().getRealRate().toString());
            }
            return object;
        });
        predictRating.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            object.setValue(String.valueOf(param.getValue().getPredictRate()));
            return object;
        });
        predictionIndex.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            object.setValue("P"+String.valueOf(param.getValue().getPredictionIndex()));
            return object;
        });
        p5.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            object.setValue(String.valueOf(param.getValue().getP5()));
            return object;
        });
    }

    public void calculRate(Long userId,Long ProductId){
        ObservableList<Similarity> similarities = catDao.getUsrSimilarList(ProductId, userId);
        ObservableList<Trust> trustSubCategory = catDao.getTrustedWithSubCategory(ProductId,new Long(1), userId);

        HashMap<Long,Double> weight =catDao.calcPrediction4(similarities,trustSubCategory);
        testRecommendation.getSelectionModel().getSelectedItem().setPredictRate(catDao.calcPredictionTest(new Long(1),null,null,null,weight,userId,ProductId));


    }
    }

