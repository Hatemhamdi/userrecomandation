/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
/**
 * FXML Controller class
 *
 * @author Hatem
 */
public class MenuController implements Initializable {
  /**
     * Initializes the controller class.
     */
   
    @FXML
    private JFXButton b1;
    @FXML
    private JFXButton b2;
    @FXML
    private JFXButton b3;
    @FXML
    private JFXButton exit;
      @FXML
    private JFXButton b5;

    @FXML
    private ImageView imgm;

    public static ImageView imgm2;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imgm.setVisible(false);
        imgm2=imgm;
        
    }    

  /*  @FXML
    private void changeColor(ActionEvent event) {
        JFXButton btn = (JFXButton) event.getSource();
       // System.out.println(btn.getText());
       switch(btn.getText())
        {
           case "Messages":
                FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource("/pg/comptable/Message.fxml").openStream());
            
        } catch (IOException e) {System.out.println(e);};
         AnchorPane root = fxmlLoader.getRoot();
         ComptableController.load1.getChildren().clear();
        ComptableController.load1.getChildren().add(root); 
        ComptableDAO cd=new ComptableDAO();
        cd.MessageVu();
        imgm.setVisible(false);
               break;
            case "Generer un Extrait":
                FXMLLoader fxmlLoader2 = new FXMLLoader();
        try {
            fxmlLoader2.load(getClass().getResource("/pg/comptable/GenererExtrait.fxml").openStream());
            
        } catch (IOException e) {System.out.println(e);};
         AnchorPane root2 = fxmlLoader2.getRoot();
         ComptableController.load1.getChildren().clear();
        ComptableController.load1.getChildren().add(root2); 
                
                //ComptableController.rootP.setStyle("-fx-background-color:#00FF00");
                break;
            case "Payée Facture"://ComptableController.rootP.setStyle("-fx-background-color:#0000FF");
               FXMLLoader fxmlLoader3 = new FXMLLoader();
        try {
            fxmlLoader3.load(getClass().getResource("/pg/comptable/PayerFacture.fxml").openStream());
            
        } catch (IOException e) {System.out.println(e);};
         AnchorPane root3 = fxmlLoader3.getRoot();
         ComptableController.load1.getChildren().clear();
        ComptableController.load1.getChildren().add(root3);
                break;
            case "Color 3":ComptableController.rootP.setStyle("-fx-background-color:#FF0000");
                break;
        }
    }*/

   /* @FXML
    private void exit(ActionEvent event) {
       // System.exit(0);
         ComptableController.isSplashLoaded=false;
        boolean isSplashLoaded1 = false;
        if(isSplashLoaded1 == false){
        try {
            
            isSplashLoaded1 = true;
            
                       

            StackPane pane = FXMLLoader.load(getClass().getResource(("Deconnexion.fxml")));
            ComptableController.rootP.getChildren().setAll(pane);

            FadeTransition fadeIn2 = new FadeTransition(Duration.seconds(0.5), pane);
            fadeIn2.setFromValue(0);
            fadeIn2.setToValue(1);
            fadeIn2.setCycleCount(1);

            FadeTransition fadeOut = new FadeTransition(Duration.seconds(0.5), pane);
            fadeOut.setFromValue(1);
            fadeOut.setToValue(0);
            fadeOut.setCycleCount(1);

            fadeIn2.play();

            fadeIn2.setOnFinished((e1) -> {
                fadeOut.play();
            });

            fadeOut.setOnFinished((e1) -> {
                try {
                    AnchorPane parentContent = FXMLLoader.load(getClass().getResource(("/pg/connexion/Connexion.fxml")));
                    ComptableController.rootP.getChildren().setAll(parentContent);

                } catch (IOException ex) {
                    Logger.getLogger(ComptableController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

        } catch (IOException ex) {
            Logger.getLogger(ComptableController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
    }*/
      
    
}
