package Category;

public class Category {

    long id;
    String name;
    long parent;

    public Category(long id, String name, long parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    public Category() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return name;
    }
}
