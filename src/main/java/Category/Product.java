package Category;

public class Product {

    Long id;
    Long idCategory;
    Float p1;
    Float p2;
    Float p3;
    Float p4;
    Float p5;
    public Product() {
    }

    public Product(Long id, Long idCategory) {
        this.id = id;
        this.idCategory = idCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }


    public Float getP1() {
        return p1;
    }

    public void setP1(Float p1) {
        this.p1 = p1;
    }

    public Float getP2() {
        return p2;
    }

    public void setP2(Float p2) {
        this.p2 = p2;
    }

    public Float getP3() {
        return p3;
    }

    public void setP3(Float p3) {
        this.p3 = p3;
    }

    public Float getP4() {
        return p4;
    }

    public void setP4(Float p4) {
        this.p4 = p4;
    }

    public Float getP5() {
        return p5;
    }

    public void setP5(Float p5) {
        this.p5 = p5;
    }

    public Product(Long id, Long idCategory, Float p1, Float p2, Float p3, Float p4) {
        this.id = id;
        this.idCategory = idCategory;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", idCategory=" + idCategory +
                '}';
    }
}
