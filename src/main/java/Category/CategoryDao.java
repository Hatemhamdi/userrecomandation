package Category;

        import dataSources.DataSource;
        import javafx.collections.FXCollections;
        import javafx.collections.ObservableList;
        import javafx.collections.transformation.SortedList;
        import object.Prediction;
        import object.Similarity;
        import object.Trust;
        import object.testTable;

        import java.sql.Connection;
        import java.sql.ResultSet;
        import java.sql.SQLException;
        import java.sql.Statement;
        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.HashMap;
        import java.util.List;
        import java.util.concurrent.atomic.AtomicReference;

public class CategoryDao {

    private Connection connection;

    public CategoryDao() {
        connection = DataSource.getInstance().getConnection();
    }

    public ObservableList<Category> CategoryList() {
        ObservableList<Category> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "select idcategory,name,parent from category where parent IS NULL";
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Category cat = new Category();

                cat.setId(rs.getLong(1));
                cat.setName(rs.getString(2));
                cat.setParent(rs.getLong(3));

                v.add(cat);
            }
            return v;

        } catch (Exception e) {
            return null;
        }
    }


    public ObservableList<Category> SubCategoryList(Long catId) {
        ObservableList<Category> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "select idcategory,name,parent from category where parent=" + catId;
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Category cat = new Category();
                cat.setId(rs.getLong(1));
                cat.setName(rs.getString(2));
                cat.setParent(rs.getLong(3));
                v.add(cat);
            }
            return v;

        } catch (Exception e) {
            return null;
        }
    }

    public ObservableList<Product> getNotRatedProductList(Long catId, Long userId) {
        ObservableList<Product> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "select DISTINCT p.idproduct,p.idcategory from product p INNER JOIN review r ON p.idproduct = r.idproduct AND p.idcategory = " + catId + " where " + userId + " NOT IN (SELECT review.iduser from review WHERE review.idproduct = p.idproduct) ORDER BY `p`.`idproduct` ASC";

        System.err.println(req);
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Product prod = new Product();
                prod.setId(rs.getLong(1));
                prod.setIdCategory(rs.getLong(2));
                v.add(prod);
            }
            return v;

        } catch (Exception e) {
            return null;
        }
    }


    public ObservableList<Similarity> getUsrSimilarList(Long productId, Long userId) {
        ObservableList<Similarity> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "select DISTINCT s.idsimilar,s.similarity from similarity s, review r where s.idsimilar = r.iduser and r.idproduct=" + productId + " and s.iduser=" + userId + " order by s.idsimilar ASC";
        System.err.println(req);
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Similarity similarity = new Similarity();
                similarity.setIdUser(userId);
                similarity.setIdSimilar(rs.getLong(1));
                similarity.setSimilarity(rs.getDouble(2));
                //similarity.setSimilarity(Double.parseDouble(new DecimalFormat("##.##").format(rs.getDouble(2))));
                v.add(similarity);
            }
            return v;

        } catch (Exception e) {
            return null;
        }
    }

    public ObservableList<Trust> getTrustedWithCategory(Long productId, Long catId, Long userId) {
        ObservableList<Trust> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "SELECT DISTINCT  t.iduser , t.idtrusted , t.trust from trust t JOIN expertise e ON t.idtrusted = e.iduser JOIN review r ON t.idtrusted = r.iduser WHERE e.idcategory = " + catId + " and t.iduser = " + userId + " and e.expertise LIKE \"category leads\" AND r.idproduct = " + productId;
        System.err.println("trust request " + req);
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Trust trust = new Trust();
                trust.setIdUser(rs.getLong(1));
                trust.setIdTrusted(rs.getLong(2));
                trust.setNbTrusters(getTrustWithCategNumerator(catId, trust.getIdTrusted()));
                v.add(trust);
            }

            long totalTrust = 0;
            for (int i = 0; i < v.size(); i++) {
                totalTrust += v.get(i).getNbTrusters();
            }

            long finalTotalTrust = totalTrust;
            v.forEach(trust -> {
                        trust.setTrust(trust.getNbTrusters() / finalTotalTrust);
                    }
            );

            return v;

        } catch (Exception e) {
            return null;
        }
    }

    public ObservableList<Trust> getTrustedWithSubCategory(Long productId, Long catId, Long userId) {
        ObservableList<Trust> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "SELECT t.iduser,t.idtrusted,t.trust from review r,trust t,product p,category c where t.iduser=" + userId + " and t.idtrusted=r.iduser and r.idproduct=p.idproduct and p.idcategory=c.idcategory  and c.idcategory=" + catId + " AND r.idproduct =" + productId + " GROUP BY t.idtrusted order by t.idtrusted ASC";
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                Trust trust = new Trust();
                trust.setIdUser(rs.getLong(1));
                trust.setIdTrusted(rs.getLong(2));
                trust.setNbTrusters(getTrustWithSubCategNumerator(catId, trust.getIdTrusted()));
                v.add(trust);
            }
            long totalTrust = 0;
            for (int i = 0; i < v.size(); i++) {
                totalTrust += v.get(i).getNbTrusters();
            }

            long finalTotalTrust = totalTrust;
            v.forEach(trust -> {
                        trust.setTrust(trust.getNbTrusters() / finalTotalTrust);
                    }
            );

            return v;

        } catch (Exception e) {
            return null;
        }
    }


    private long getTrustWithCategNumerator(Long catId, Long idTrusted) {
        ResultSet rs;
        Statement ps;
        String reqNbTrusters = "SELECT count(t.iduser) from trust t JOIN expertise e ON t.idtrusted = e.iduser WHERE e.idcategory = " + catId + " and t.idtrusted =" + idTrusted;
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(reqNbTrusters);
            while (rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private long getTrustWithSubCategNumerator(Long catId, Long idTrusted) {
        ResultSet rs;
        Statement ps;
        String reqNbTrusters = "SELECT count( DISTINCT t.iduser) from trust t JOIN review r ON t.iduser = r.iduser JOIN product p ON r.idproduct = p.idproduct WHERE t.idtrusted = " + idTrusted + " AND p.idcategory = " + catId;

        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(reqNbTrusters);
            while (rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public float calcPrediction(Long catId, ObservableList<Similarity> similarities, ObservableList<Trust> trustsCateg, ObservableList<Trust> trustsSubCateg,HashMap<Long,Double> weight,
                                long userId, long productId) {

        ResultSet rs;
        Statement ps;
        String ratingAverage = "SELECT AVG(r.rating) FROM review r JOIN product p ON p.idproduct=r.idproduct WHERE r.iduser = "+userId+" AND p.idcategory="+catId;

        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(ratingAverage);
            while (rs.next()) {
                float avg = rs.getFloat(1);
                AtomicReference<Float> sommeSimilarity = new AtomicReference<>(0f);
                AtomicReference<Float> rateELLAaverage = new AtomicReference<>(0f);

                if(similarities != null)
                    similarities.forEach(similarity -> {
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getSimilarity())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdSimilar(), productId) - averageOfSimilar(catId, similarity.getIdSimilar()))*similarity.getSimilarity())));

                    });
                else if (trustsCateg!= null)
                    trustsCateg.forEach(similarity -> {
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdTrusted(), productId) - averageOfSimilar(catId, similarity.getIdTrusted()))*similarity.getTrust())));
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getTrust()))); });
                else if(trustsSubCateg != null)
                    trustsSubCateg.forEach(similarity -> {
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getTrust())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdTrusted(), productId) - averageOfSimilar(catId, similarity.getIdTrusted()))*similarity.getTrust())));
                    });

                else if(weight != null)
                    for (HashMap.Entry<Long, Double> entry : weight.entrySet()) {
                        System.out.println(entry.getKey() + " = " + entry.getValue());
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + entry.getValue())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(entry.getKey(), productId) - averageOfSimilar(catId, entry.getKey()))*entry.getValue())));
                    }



                System.err.println("value: "+avg + ((rateELLAaverage.get())/sommeSimilarity.get()));
                return avg + ((rateELLAaverage.get())/sommeSimilarity.get());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private float averageOfSimilar(Long catId, long userId) {

        ResultSet rs;
        Statement ps;
        String ratingAverage = "SELECT AVG(r.rating) FROM review r JOIN product p ON p.idproduct=r.idproduct WHERE r.iduser = "+userId+" AND p.idcategory="+catId;

        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(ratingAverage);
            while (rs.next()) {
                float avg = rs.getFloat(1);
                return avg;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;


    }

    private float functionRates(Long idSimilar, long productId) {
        ResultSet rs;
        Statement ps;
        String ratingAverage = "SELECT r.rating FROM review r WHERE r.iduser = "+idSimilar+" AND r.idproduct = "+ productId;

        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(ratingAverage);
            while (rs.next()) {
                return rs.getFloat(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public HashMap<Long, Double> calcPrediction4(ObservableList<Similarity> similarities, ObservableList<Trust> trustSubCategory) {
        HashMap<Long,Double> weight = new HashMap<>();
        trustSubCategory.forEach(
                trust -> {
                    Similarity s = similarities.stream().filter(t -> t.getIdSimilar().equals(trust.getIdTrusted())).findFirst().orElse(null);

                    if(s!= null && s.getIdSimilar() != 0){
                        weight.put (s.getIdSimilar(),(2*s.getSimilarity()*trust.getTrust())/(s.getSimilarity()+trust.getTrust()));
                    }
                });
        return weight;
    }

    public ObservableList<Prediction> calFinalPrediction(long catId, String nbrUser, String nbrProduct) {
        ObservableList<Prediction> predictions = FXCollections.observableArrayList();
       for (int i = 0 ; i< Integer.valueOf(nbrUser) ; i++){
           ResultSet rs;
           Statement ps;
           String ratingAverage = "\n" +
                   "select DISTINCT p.idproduct,p.idcategory from product p INNER JOIN review r ON p.idproduct = r.idproduct AND p.idcategory = 1 where 1 NOT IN (SELECT review.iduser from review WHERE review.idproduct = p.idproduct) ORDER BY RAND() LIMIT "+nbrProduct;

           try {
               ps = connection.createStatement();
               rs = ps.executeQuery(ratingAverage);
               List<Prediction> tempPredictions = new ArrayList<>();

               while (rs.next()) {
                   Prediction prediction = new Prediction();
                   prediction.setCatId(catId);
                   prediction.setProdId(rs.getLong(1));
                   prediction.setIdUser(new Long(i+1));
                   prediction.setPred(calcFinalPrediction(prediction.getProdId(),catId,prediction.getIdUser()));
                   tempPredictions.add(prediction);
               }
               Collections.sort(tempPredictions);
               predictions.addAll(tempPredictions);
           } catch (SQLException e) {
               e.printStackTrace();
           }
       }
       return predictions;
    }

    private float calcFinalPrediction(long productId,long catId,long userId) {
        ObservableList<Trust> trustObservableList = getTrustedWithSubCategory(productId,catId, userId);
        return calcPrediction(catId,null,null,trustObservableList,null,userId,productId);
    }


    public ObservableList<testTable> getAllTestTable() {
        ObservableList<testTable> v = FXCollections.observableArrayList();
        ResultSet rs;
        Statement ps;
        String req = "select * from hidetestset";
        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(req);
            while (rs.next()) {
                testTable obj = new testTable();

                obj.setIdUser(rs.getLong(1));
                obj.setProdId(rs.getLong(2));
                obj.setRealRate(rs.getLong(3));


                    float result;
                    result=calculRate(4, obj.getIdUser(), obj.getProdId());
               // if (!Float.isNaN(result)) {
                    obj.setPredictRate(result);
                    obj.setPredictionIndex(4);
                /*} else {
                    result=calculRate(3, obj.getIdUser(), obj.getProdId());
                    if (!Float.isNaN(result)){
                    obj.setPredictionIndex(3);
                    obj.setPredictRate(result);
            }

                else {

                        result = calculRate(2, obj.getIdUser(), obj.getProdId());
                        if (!Float.isNaN(result)) {
                            obj.setPredictionIndex(2);
                            obj.setPredictRate(result);
                        } else {


                            obj.setPredictionIndex(1);
                            obj.setPredictRate(calculRate(1, obj.getIdUser(), obj.getProdId()));
                        }
                    }}*/
                obj.setP5(calculateP5(5,obj.getIdUser(),obj.getProdId()));
                v.add(obj);
            }
            return v;

        } catch (Exception e) {
            return null;
        }
    }


    public float calcPredictionTest(Long catId, ObservableList<Similarity> similarities, ObservableList<Trust> trustsCateg, ObservableList<Trust> trustsSubCateg,HashMap<Long,Double> weight,
                                long userId, long productId) {

        ResultSet rs;
        Statement ps;
        String ratingAverage = "SELECT AVG(r.rating) FROM testset r JOIN product p ON p.idproduct=r.idproduct WHERE r.iduser = "+userId+" AND p.idcategory="+catId;

        try {
            ps = connection.createStatement();
            rs = ps.executeQuery(ratingAverage);
            while (rs.next()) {
                float avg = rs.getFloat(1);
                AtomicReference<Float> sommeSimilarity = new AtomicReference<>(0f);
                AtomicReference<Float> rateELLAaverage = new AtomicReference<>(0f);

                if(similarities != null)
                    similarities.forEach(similarity -> {
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getSimilarity())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdSimilar(), productId) - averageOfSimilar(catId, similarity.getIdSimilar()))*similarity.getSimilarity())));

                    });
                else if (trustsCateg!= null)
                    trustsCateg.forEach(similarity -> {
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdTrusted(), productId) - averageOfSimilar(catId, similarity.getIdTrusted()))*similarity.getTrust())));
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getTrust()))); });
                else if(trustsSubCateg != null)
                    trustsSubCateg.forEach(similarity -> {
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + similarity.getTrust())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(similarity.getIdTrusted(), productId) - averageOfSimilar(catId, similarity.getIdTrusted()))*similarity.getTrust())));
                    });

                else if(weight != null)
                    for (HashMap.Entry<Long, Double> entry : weight.entrySet()) {
                        System.out.println(entry.getKey() + " = " + entry.getValue());
                        sommeSimilarity.updateAndGet(v -> new Float((float) (v + entry.getValue())));
                        rateELLAaverage.updateAndGet(v -> new Float((float) (v + (functionRates(entry.getKey(), productId) - averageOfSimilar(catId, entry.getKey()))*entry.getValue())));
                    }



                System.err.println("value: "+avg + ((rateELLAaverage.get())/sommeSimilarity.get()));
                return avg + ((rateELLAaverage.get())/sommeSimilarity.get());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }


    public float calculRate(int p,Long userId,Long ProductId){
        ObservableList<Similarity> similarities = getUsrSimilarList(ProductId, userId);
        ObservableList<Trust> trustSubCategory = getTrustedWithSubCategory(ProductId,new Long(1), userId);

        HashMap<Long,Double> weight =calcPrediction4(similarities,trustSubCategory);
            float result;
        //if(p==4) {
            result=(calcPredictionTest(new Long(1), null, null, null, weight, userId, ProductId));
                return result;
        //}

        /*else if(p==3) {
            result= (calcPrediction(new Long(1), null, null, trustSubCategory, null, userId, ProductId));
            if(result>5)
                return  5;
            else if(result<0)
                return 0;
            else
                return result;
        }else if(p==2) {
            ObservableList<Trust> trustCategory = getTrustedWithCategory(ProductId,new Long(1),userId);
            result= (calcPrediction(new Long(1), null, trustCategory, null, null, userId, ProductId));
            if(result>5)
                return  5;
            else if(result<0)
                return 0;
            else
                return result;
        }
        else {
            result= (calcPrediction(new Long(1), similarities, null, null, null, userId, ProductId));

            if(result>5)
                return  5;
            else if(result<0)
                return 0;
            else
                return result;
        }*/


    }

    private float calculateP5(int p,Long userId,Long ProductId){
            ObservableList<Similarity> similarities = getUsrSimilarList(ProductId, userId);
        ObservableList<Trust> trustCategory = getTrustedWithCategory(ProductId,new Long(1), userId);

            HashMap<Long,Double> weight =calcPrediction4(similarities,trustCategory);
            float result;
            //if(p==4) {
            result=(calcPredictionTest(new Long(1), similarities, null, null, null, userId, ProductId));
            return result;
    }
}
