import Category.Category;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import Category.CategoryDao;
import javafx.scene.layout.StackPane;
import object.Prediction;

import java.net.URL;
import java.util.ResourceBundle;

public class Interface2 implements Initializable {

    @FXML
    private StackPane root;

    @FXML
    private JFXComboBox<Category> comboCat;

    @FXML
    private JFXComboBox<Category> comboSubCat;

    @FXML
    private JFXTextField txtUserId;

    @FXML
    private JFXTextField txtProd;

    @FXML
    private JFXButton submitButton;


    @FXML
    private TableView<Prediction> tabRecommendation;

    @FXML
    private TableColumn<Prediction, String> colCat;

    @FXML
    private TableColumn<Prediction, String> colUser;

    @FXML
    private TableColumn<Prediction, String> colProduct;

    @FXML
    private TableColumn<Prediction, String> colPred;

    @FXML
    private JFXSpinner progressProduct;

    CategoryDao catDao = new CategoryDao();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        submitButton.setOnAction(e->{
            tabRecommendation.setItems(catDao.calFinalPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),txtUserId.getText(),txtProd.getText()));
        });
        comboCat.setItems(catDao.CategoryList());
        comboSubCat.getItems().add(new Category(0, "None", 0));

        comboSubCat.getSelectionModel().selectFirst();
        comboCat.getSelectionModel().selectFirst();

        comboSubCat.setItems(catDao.SubCategoryList(comboCat.getSelectionModel().getSelectedItem().getId()));
        comboSubCat.getSelectionModel().selectFirst();

        comboCat.setOnAction(e -> {
            comboSubCat.setItems(catDao.SubCategoryList(comboCat.getSelectionModel().getSelectedItem().getId()));
            comboSubCat.getSelectionModel().selectFirst();
        });
        initTables();
    }


    private void initTables() {
        colCat.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getCatId() != null) {
                object.setValue(param.getValue().getCatId().toString());
            }
            return object;
        });
        colUser.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdUser() != null) {
                object.setValue(param.getValue().getIdUser().toString());
            }
            return object;

        });

        colProduct.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getProdId() != null) {
                object.setValue(param.getValue().getProdId().toString());
            }
            return object;
        });
        colPred.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            object.setValue(String.valueOf(param.getValue().getPred()));
            return object;
        });
    }



    }
