package object;

public class Weight {
    Long idUser;
    Long idSimilar;
    Double Similarity;

    public Weight(Long idUser, Long idSimilar, Double similarity) {
        this.idUser = idUser;
        this.idSimilar = idSimilar;
        Similarity = similarity;
    }
    public Weight(){

    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdSimilar() {
        return idSimilar;
    }

    public void setIdSimilar(Long idSimilar) {
        this.idSimilar = idSimilar;
    }

    public Double getSimilarity() {
        return Similarity;
    }

    public void setSimilarity(Double similarity) {
        Similarity = similarity;
    }


    @Override
    public String toString() {
        return "Similarity{" +
                "idUser=" + idUser +
                ", idSimilar=" + idSimilar +
                ", similarity=" + Similarity +
                '}';
    }
}
