package object;

public class Similarity {
    Long idUser;
    Long idSimilar;
    Double Similarity;

    public Similarity(Long idUser, Long idSimilar, Double similarity) {
        this.idUser = idUser;
        this.idSimilar = idSimilar;
        Similarity = similarity;
    }
    public Similarity(){

    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdSimilar() {
        return idSimilar;
    }

    public void setIdSimilar(Long idSimilar) {
        this.idSimilar = idSimilar;
    }

    public Double getSimilarity() {
        return Similarity;
    }

    public void setSimilarity(Double similarity) {
        Similarity = similarity;
    }


    @Override
    public String toString() {
        return "Similarity{" +
                "idUser=" + idUser +
                ", idSimilar=" + idSimilar +
                ", similarity=" + Similarity +
                '}';
    }
}
