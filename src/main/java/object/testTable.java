package object;

public class testTable {
    Long idUser;
    Long realRate;
    Long prodId;
    float predictRate;
    float p5;

    public int getPredictionIndex() {
        return predictionIndex;
    }

    public void setPredictionIndex(int predictionIndex) {
        this.predictionIndex = predictionIndex;
    }

    int predictionIndex;


    /**
     *  this represents the prediction
     *
     *
     * */

    public testTable() {
    }

    public testTable(Long idUser, Long realRate, Long prodId, float predictRate, float p5, int predictionIndex) {
        this.idUser = idUser;
        this.realRate = realRate;
        this.prodId = prodId;
        this.predictRate = predictRate;
        this.p5 = p5;
        this.predictionIndex = predictionIndex;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getRealRate() {
        return realRate;
    }

    public void setRealRate(Long realRate) {
        this.realRate = realRate;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public float getPredictRate() {
        return predictRate;
    }

    public void setPredictRate(float predictRate) {
        this.predictRate = predictRate;
    }

    public float getP5() {
        return p5;
    }

    public void setP5(float p5) {
        this.p5 = p5;
    }
}


