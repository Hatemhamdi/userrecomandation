package object;

public class Trust {
    Long idUser;
    Long idTrusted;
    float trust;


    /**
     *  this represents the number of trusters of the given trusted user
     *
     * */
    float nbTrusters ;

    public Trust(Long idUser, Long idTrusted, float trust) {
        this.idUser = idUser;
        this.idTrusted = idTrusted;
        this.trust = trust;
    }
    public Trust(){}

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }



    public float getNbTrusters() {
        return nbTrusters;
    }

    public void setNbTrusters(float nbTrusters) {
        this.nbTrusters = nbTrusters;
    }

    public Long getIdTrusted() {
        return idTrusted;
    }

    public void setIdTrusted(Long idTrusted) {
        this.idTrusted = idTrusted;
    }

    public float getTrust() {
        return trust;
    }

    public void setTrust(float trust) {
        this.trust = trust;
    }

    @Override
    public String toString() {
        return "Trust{" +
                "idUser=" + idUser +
                ", idTrusted=" + idTrusted +
                ", trust=" + trust +
                '}';
    }
}
