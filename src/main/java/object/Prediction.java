package object;

public class Prediction implements Comparable{
    Long idUser;
    Long catId;
    Long prodId;


    /**
     *  this represents the prediction
     *
     *
     * */
    float pred;

    public Prediction() {
    }

    public Prediction(Long idUser, Long catId, Long prodId, float pred) {
        this.idUser = idUser;
        this.catId = catId;
        this.prodId = prodId;
        this.pred = pred;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public float getPred() {
        return pred;
    }

    public void setPred(float pred) {
        this.pred = pred;
    }

    @Override
    public int compareTo(Object o) {
        float compareage=((Prediction)o).getPred();
        /* For Ascending order*/
        return Math.round(compareage-this.pred);
    }
}
