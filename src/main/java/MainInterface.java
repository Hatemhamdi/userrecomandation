import Category.Category;
import Category.CategoryDao;
import Category.Product;
import com.jfoenix.controls.*;
import dataSources.DataSource;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import object.Similarity;
import object.Trust;

import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class MainInterface implements Initializable {

    @FXML
    private StackPane root;

    @FXML
    private JFXComboBox<Category> comboCat;

    @FXML
    private JFXComboBox<Category> comboSubCat;

    @FXML
    private JFXTextField txtUserId;

    @FXML
    private JFXButton submitButton;



    public JFXSpinner getProgressProduct() {
        return progressProduct;
    }

    @FXML
    private JFXSpinner progressProduct;

    @FXML
    private TableView<Product> tabSubCatNotRated;

    @FXML
    private TableColumn<Product, String> colProduct;

    @FXML
    private TableColumn<Product, String> colCat;
    @FXML
    private TableColumn<Product, String> p1;
    @FXML
    private TableColumn<Product, String> p2;
    @FXML
    private TableColumn<Product, String> p3;
    @FXML
    private TableColumn<Product, String> p4;
    @FXML
    private TableColumn<Product, String> p5;
    @FXML
    private TableView<Similarity> tabSubCategorySimilar;

    @FXML
    private TableColumn<Similarity, String> colUser;

    @FXML
    private TableColumn<Similarity, String> colSimID;

    @FXML
    private TableColumn<Similarity, String> colSim;

    @FXML
    private TableView<Trust> tabTrustWithCategory;

    @FXML
    private TableColumn<Trust, String> colUserTrust;

    @FXML
    private TableColumn<Trust, String> colTrusID;

    @FXML
    private TableColumn<Trust, String> colTrust;

    @FXML
    private TableView<Trust> tabTrustWithCategory2;

    @FXML
    private TableColumn<Trust, String> colUserTrust2;

    @FXML
    private TableColumn<Trust, String> colTrusID2;

    @FXML
    private TableColumn<Trust, String> colTrust2;

    CategoryDao catDao = new CategoryDao();

    private Connection connection;

    Float minPrediction4 = new Float(0);
    Float maxPrediction4 = new Float(5);


    public MainInterface() {
        connection = DataSource.getInstance().getConnection();
        System.err.println(connection == null);
    }

    public void initialize(URL location, ResourceBundle resources) {
        progressProduct.setRadius(100);
        initTables();


        Task<Void> task=new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                comboCat.setItems(catDao.CategoryList());
                return null;
            }
        };

        progressProduct.visibleProperty().bind(task.runningProperty());
        root.disableProperty().bind(task.runningProperty());

        task.setOnSucceeded(a->{
            comboSubCat.getItems().add(new Category(0, "None", 0));

            comboSubCat.getSelectionModel().selectFirst();
            comboCat.getSelectionModel().selectFirst();

            comboSubCat.setItems(catDao.SubCategoryList(comboCat.getSelectionModel().getSelectedItem().getId()));
            comboSubCat.getSelectionModel().selectFirst();
        });

        Platform.runLater(()-> {
                    new Thread(task).start();
                });



        comboCat.setOnAction(e -> {
            comboSubCat.setItems(catDao.SubCategoryList(comboCat.getSelectionModel().getSelectedItem().getId()));
            comboSubCat.getSelectionModel().selectFirst();
        });
        submitButton.setOnAction(e -> {
            btnSubmitAction();
        });


        tabSubCatNotRated.setOnMouseClicked(action -> {
            if (tabSubCatNotRated.getSelectionModel().getSelectedItem() != null) {
                ObservableList<Similarity> similarities = catDao.getUsrSimilarList(tabSubCatNotRated.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText()));
                ObservableList<Trust> trustCategory = catDao.getTrustedWithCategory(tabSubCatNotRated.getSelectionModel().getSelectedItem().getId(),comboCat.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText()));
                ObservableList<Trust> trustSubCategory = catDao.getTrustedWithSubCategory(tabSubCatNotRated.getSelectionModel().getSelectedItem().getId(),comboSubCat.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText()));
                tabSubCategorySimilar.setItems(similarities);
                tabTrustWithCategory.setItems(trustCategory);
                tabTrustWithCategory2.setItems(trustSubCategory);


                tabSubCatNotRated.getSelectionModel().getSelectedItem().setP1(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),similarities,null,null,null,Long.parseLong(txtUserId.getText()),tabSubCatNotRated.getSelectionModel().getSelectedItem().getId()));
                tabSubCatNotRated.getSelectionModel().getSelectedItem().setP2(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),null,trustCategory,null,null,Long.parseLong(txtUserId.getText()),tabSubCatNotRated.getSelectionModel().getSelectedItem().getId()));
                tabSubCatNotRated.getSelectionModel().getSelectedItem().setP3(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),null,null,trustSubCategory,null,Long.parseLong(txtUserId.getText()),tabSubCatNotRated.getSelectionModel().getSelectedItem().getId()));
                HashMap<Long,Double> weight =catDao.calcPrediction4(similarities,trustSubCategory);
                tabSubCatNotRated.getSelectionModel().getSelectedItem().setP4(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),null,null,null,weight,Long.parseLong(txtUserId.getText()),tabSubCatNotRated.getSelectionModel().getSelectedItem().getId()));
                weight =catDao.calcPrediction4(similarities,trustCategory);
                tabSubCatNotRated.getSelectionModel().getSelectedItem().setP5(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(),null,null,null,weight,Long.parseLong(txtUserId.getText()),tabSubCatNotRated.getSelectionModel().getSelectedItem().getId()));


                System.err.println("p11111: "+ tabSubCatNotRated.getSelectionModel().getSelectedItem().getP1());
                List<Product> products=FXCollections.observableArrayList();
                products.addAll( tabSubCatNotRated.getItems());
                tabSubCatNotRated.getItems().clear();
                tabSubCatNotRated.getItems().addAll(products);
            }



        });
        tabSubCatNotRated.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {
            @Override
            public void changed(ObservableValue<? extends Product> observable, Product oldValue, Product newValue) {
                if(oldValue!=newValue)
                    if (tabSubCatNotRated.getSelectionModel().getSelectedItem() != null) {
                        tabSubCategorySimilar.setItems(catDao.getUsrSimilarList(tabSubCatNotRated.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText())));
                    }
            }
        });
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    private void btnSubmitAction() {
        Task<Void> task=new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                if (!txtUserId.getText().isEmpty()) {
                    tabSubCatNotRated.setItems(catDao.getNotRatedProductList(comboSubCat.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText())));
                    System.err.println("tab size: "+tabSubCatNotRated.getItems().size());
            }
                return null;
            }
        };





        progressProduct.visibleProperty().bind(task.runningProperty());
        root.disableProperty().bind(task.runningProperty());
        Platform.runLater(()->{
            new Thread(task).start();
       /* task.setOnSucceeded(tsk->{
            for (Product e : tabSubCatNotRated.getItems()) {
                ObservableList<Similarity> similarities = catDao.getUsrSimilarList(e.getId(), Long.parseLong(txtUserId.getText()));
                ObservableList<Trust> trustCategory = catDao.getTrustedWithCategory(e.getId(), comboCat.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText()));
                ObservableList<Trust> trustSubCategory = catDao.getTrustedWithSubCategory(e.getId(), comboSubCat.getSelectionModel().getSelectedItem().getId(), Long.parseLong(txtUserId.getText()));
                HashMap<Long, Double> weight = catDao.calcPrediction4(similarities, trustSubCategory);
                e.setP4(catDao.calcPrediction(comboSubCat.getSelectionModel().getSelectedItem().getId(), null, null, null, weight, Long.parseLong(txtUserId.getText()), e.getId()));
            }
        });*/
        });
progressProduct.visibleProperty().bind(task.runningProperty());


    }

    private void initTables() {
        colProduct.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getId() != null) {
                object.setValue(param.getValue().getId().toString());
            }
            return object;
        });
        colCat.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdCategory() != null) {
                object.setValue(param.getValue().getIdCategory().toString());
            }
            return object;

        });

        p1.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getP1() != null) {
                object.setValue(param.getValue().getP1().toString());
            }
            return object;

        });
        p2.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getP2() != null) {
                object.setValue(param.getValue().getP2().toString());
            }
            return object;

        });
        p3.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getP3() != null) {
                object.setValue(param.getValue().getP3().toString());
            }
            return object;

        });

        p4.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getP4() != null) {
                object.setValue(param.getValue().getP4().toString());
            }
            return object;

        });

        p5.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getP5() != null) {
                object.setValue(param.getValue().getP5().toString());
            }
            return object;

        });

        colUser.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdUser() != null) {
                object.setValue(param.getValue().getIdUser().toString());
            }
            return object;
        });
        colSimID.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdSimilar() != null) {
                object.setValue(param.getValue().getIdSimilar().toString());
            }
            return object;

        });

        colSim.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getSimilarity() != null) {
                object.setValue(param.getValue().getSimilarity().toString());
            }
            return object;
        });
        colUserTrust.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdUser() != null) {
                object.setValue(param.getValue().getIdUser().toString());
            }
            return object;

        });

        colTrusID.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdTrusted() != null) {
                object.setValue(param.getValue().getIdTrusted().toString());
            }
            return object;
        });
        colTrust.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
                object.setValue(String.valueOf(param.getValue().getTrust()));

            return object;

        });

        colUserTrust2.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdUser() != null) {
                object.setValue(param.getValue().getIdUser().toString());
            }
            return object;

        });

        colTrusID2.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            if (param.getValue().getIdTrusted() != null) {
                object.setValue(param.getValue().getIdTrusted().toString());
            }
            return object;
        });
        colTrust2.setCellValueFactory(param -> {
            StringProperty object = new SimpleStringProperty();
            object.setValue(String.valueOf(param.getValue().getTrust()));

            return object;

        });
    }
}
