import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;


public class HomeController implements Initializable{

    @FXML
    private JFXDrawer drawer;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private AnchorPane load;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            VBox box = FXMLLoader.load(getClass().getResource("menu.fxml"));
            drawer.setSidePane(box);
        } catch (IOException ex) {
            System.err.println("exception "+ ex.getMessage());
        }

        try {
            StackPane pane = FXMLLoader.load(getClass().getResource(("/MainInterface.fxml")));
            load.getChildren().setAll(pane);
            AnchorPane.setTopAnchor(pane,0.0);
            AnchorPane.setBottomAnchor(pane,0.0);
            AnchorPane.setLeftAnchor(pane,0.0);
            AnchorPane.setRightAnchor(pane,0.0);
        } catch (IOException ex) {
            System.err.println("exception "+ ex.getMessage());
        }

        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(hamburger);
        transition.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            transition.setRate(transition.getRate()*-1);
            transition.play();



            if(drawer.isShown())
            {
                drawer.close();
            }else
                drawer.open();
        });
    }
}
